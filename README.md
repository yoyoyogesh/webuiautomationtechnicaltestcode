### QA Automation Engineer - GlobalKinetic

### Web Automation with Selenium

### Pre-requisites
 IDE - IntelliJ
 Java version -16.0.1 
 Build tool - Maven build tool
 Test Design approach - BDD framework using cucumber tool where page object model design techniques is used to deal with web elements
                 Benefits of Framework:
				                1. Reusability
								2. Less cost required in order to design and development of framework.
								3. Maintenance is easy
								4. Code quality is achieved by using SonarLint and Sonarqube 
		
### Guidelines on how to execute your tests and how to view reporting 
Local Execution: Clone this project on your machine (windows preferrably ) and open in any IDE like Intellij/Eclipse. follow maven cycle and run test through TestRunner file.
Execution in pipeline: configure jenkins for maven project and execute test in pipeline by clicking Build now button.
Reports: test execution report can be found target >> cucumber-reports >> index.html

#### Task
Requirement: 
• Demonstrate functional web automation testing using Selenium WebDriver 
System Under test: 
Use the following website as the system under test: 
https://www.saucedemo.com/ 

Note: Its better to use realtime website instead of demo website.

Criteria: 
• Demonstrate functional testing & basic navigation between pages 
• Implement best practices for frontend automation code 
• Element locator strategies that are based on best practices 
• Use of parameterisation and variables where possible 
• Use of assertions on each test based on best practices 


#### How criteria achieved 
- Created feature file using Gherkin syntax
- Createx step definitions and link these to the feature file
- Written implementation which links both pieces up
- Createx TestRunner to run test
- followed coding standards
- Assert statements used for validations. 


