package pages;

import org.junit.Assert;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class CartPage
{
    private WebDriver driver;

    //Cart Page elements
    @FindBy(xpath = "//*[@id=\"search\"]/div[1]/div/div[1]/div/span[3]/div[2]/div[3]/div/span/div/div/div/div")
    private WebElement selectSearchedProduct;

    @FindBy(css = "body")
    private WebElement pageElement;

    @FindBy(css="#quantity")
    private WebElement cartquantitydropdown;

    @FindBy(css="#add-to-cart-button")
    private WebElement cartBtn;

    @FindBy(css="#nav-cart-count-container")
    private WebElement cartvalue;

    @FindBy(xpath = "//*[@id=\"sc-item-C96444909-6a98-470b-b3f1-f7c1245d513a\"]/div[4]/div/div[1]/div/div/div[2]/ul/li[1]/span/a/span[1]/span/span[2]")
    private WebElement productname;

public CartPage(WebDriver driver)
{
    this.driver=driver;
    PageFactory.initElements(driver, this);
}

public void clickOnSearchedProduct()
{
    selectSearchedProduct.click();
}

public void clickAddToCartButton()
{
    cartBtn.click();
}

public WebElement  CartContainerValue()
{
  return  cartvalue;
}

public WebElement cartQuantity()
{
    return cartquantitydropdown;
}

    //navigate page prior to Tab Switch
    public void navigatetoNewPage()
    {
        pageElement.sendKeys(Keys.CONTROL, Keys.PAGE_DOWN);
    }

    public void clickCartContainer()
    {
        cartvalue.click();
    }
//Product name
    public WebElement productName()
    {
        return productname;
    }

}
