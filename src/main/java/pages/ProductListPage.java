package pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import java.util.concurrent.TimeUnit;

public class ProductListPage
{
    private WebDriver driver;

    //Product page elements
    @FindBy(xpath = "//select[@name=\"url\"]")
    private WebElement dropdownelement;

    @FindBy(id = "twotabsearchtextbox")
    private WebElement txtsearchbox;

    public ProductListPage(WebDriver driver)
    {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public WebElement selectdropdowncategory()
    {
      return dropdownelement;
    }

    public void enterTxtSearchbox(String strproductname)
    {
        txtsearchbox.sendKeys(strproductname);

    }


}
