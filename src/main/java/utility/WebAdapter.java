package utility;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.*;

import java.util.ArrayList;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

public class WebAdapter {
    WebDriver driver;

    public WebAdapter(WebDriver driver) {
        this.driver = driver;
    }

    public void launchApp(String appUrl) {
        driver.get(appUrl);
    }

    public void maximizewindow() {
        driver.manage().window().maximize();
    }

    //Switch Tab
    public void switchToTab(int tabnumber) {
        ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
        driver.switchTo().window(tabs.get(tabnumber));
    }

    //select Dropdown value
    public void dropdownValue(WebElement webElement, String value) {
        Select select = new Select(webElement);
        select.selectByVisibleText(value);
    }

    //Implicit wait
    public void implicitwait(long waittime) {
        driver.manage().timeouts().implicitlyWait(waittime, TimeUnit.SECONDS);
    }

    //Explicit wait
    public void explicitWaitForelement(String locatorCss, long milliseconds) {
        WebDriverWait explicitWait = new WebDriverWait(driver, milliseconds);
        explicitWait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(locatorCss)));
    }

    //Fluent Wait
public void fluentWait()
{
    Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)
            .withTimeout(30, TimeUnit.SECONDS)
            .pollingEvery(5, TimeUnit.SECONDS)
            .ignoring(NoSuchElementException.class);
    WebElement element = wait.until(new Function<WebDriver, WebElement>(){

        public WebElement apply(WebDriver driver ) {
            return driver.findElement(By.cssSelector("#quantity"));
        }
    });

}

    //Press Enter Key
    public void keyboardEnterKey()
    {
        Actions action = new Actions(driver);
        action.sendKeys(Keys.ENTER).build().perform();
    }

    //Get Text
    public String getText(WebElement webElement)
    {
    try {
            return webElement.getText();
         }catch (Exception e)
    {
        e.getStackTrace();
    }
    return null;
    }

    //Assert the Value
    public void assertEquals(String expectedValue, String actualValue)
    {
        Assert.assertEquals("Required assert failed", "1", actualValue);
    }

    //Assert True
    public void assertTrue(int count)
    {
        Assert.assertTrue("count is not correct",count>=1);
    }

    //Close the browser
    public void closebrowser()
    { try
    { }catch (Exception e)
    {
        e.getMessage();
    }finally {
        driver.quit();
    }
    }
}
