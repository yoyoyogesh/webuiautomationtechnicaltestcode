@Regression @Scenario1
  Feature: Online Shopping
    Scenario Outline: As a user, I would like to do online shopping
      Given user is ready with required information
      When product searched correctly and added into cart "<productname>"
      Then verify that product is present in the cart
      Examples:
      |productname|
      |Cloth Mask           |