/*
package StepDefinition;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumber.api.java.en.Then;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.security.Key;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class SearchProduct
{
private WebDriver driver;

    @Given("^user is ready with required information$")
    public void user_is_ready_with_required_information() throws Throwable {
        //Set Browser
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.setExperimentalOption("useAutomationExtension", false);
        System.setProperty("webdriver.chrome.driver", "src" + File.separator  + "main" + File.separator + "resources" + File.separator  + "Drivers" + File.separator + "chromedriver.exe");
        driver = new ChromeDriver(chromeOptions);

        //Lunch application
        driver.get("https://www.amazon.in/");

        //Maximize the browser window
        driver.manage().window().maximize();
    }

    @When("^product searched correctly and added into cart \"([^\"]*)\"$")
    public void product_searched_correctly_and_added_into_cart(String strproductname) throws Throwable {
        //select category to search product
        WebElement dropdownelement = driver.findElement(By.xpath("//select[@name=\"url\"]"));
        Select dropdown = new Select(dropdownelement);
        dropdown.selectByVisibleText("Clothing & Accessories");

        //Search Product
        driver.findElement(By.id("twotabsearchtextbox")).sendKeys(strproductname);

        //Press Enter Key
        Actions action = new Actions(driver);
        action.sendKeys(Keys.ENTER).build().perform();

        //Wait for page to load
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        //Add product into cart
        driver.findElement(By.xpath("//*[@id=\"search\"]/div[1]/div/div[1]/div/span[3]/div[2]/div[3]")).click();
        driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL, Keys.PAGE_DOWN);

        //Switch to new Tab
        ArrayList<String> tabs2 = new ArrayList<String> (driver.getWindowHandles());
      */
/*  driver.switchTo().window(tabs2.get(0));
        driver.close();*//*

        driver.switchTo().window(tabs2.get(1));

        //Explicit wait
      WebDriverWait explicitwait =new WebDriverWait(driver, 20);
      explicitwait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#add-to-cart-button")));
        driver.findElement(By.cssSelector("#add-to-cart-button")).click();
    }

    @Then("^verify that product is present in the cart$")
    public void verify_that_product_is_present_in_the_cart() throws Throwable {
        //wait for page to be added in cart
        WebDriverWait explicitwait = new WebDriverWait(driver,30);
        explicitwait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#nav-cart-count-container")));
      String strcartvalue = driver.findElement(By.cssSelector("#nav-cart-count-container")).getText();
      Assert.assertEquals("Product is not added into the cart",  "1",strcartvalue);

        //Close the browser
        driver.quit();
    }

}
*/
