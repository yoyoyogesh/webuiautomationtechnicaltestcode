package StepDefinition;

import org.openqa.selenium.WebElement;
import utility.WebAdapter;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import pages.CartPage;
import pages.ProductListPage;

import java.io.File;

public class AddtoCartStep {
    private WebDriver driver;


    @Given("^user is ready with required information$")
    public void user_is_ready_with_required_information() throws Throwable {
     //Browser setup
     ChromeOptions chromeOptions = new ChromeOptions();
    chromeOptions.setExperimentalOption("useAutomationExtension",false);
     System.setProperty("webdriver.chrome.driver", "src" + File.separator  + "main" + File.separator + "resources" + File.separator  + "Drivers" + File.separator + "chromedriver.exe");
     driver = new ChromeDriver(chromeOptions);
     WebAdapter webAdapter = new WebAdapter(driver);
     webAdapter.launchApp("https://www.amazon.in/");
     webAdapter.maximizewindow();
    }

    @When("^product searched correctly and added into cart \"([^\"]*)\"$")
    public void product_searched_correctly_and_added_into_cart(String productname) throws Throwable {
     ProductListPage productListPage = new ProductListPage(driver);
    WebElement dropdownElement = productListPage.selectdropdowncategory();
     WebAdapter webAdapter = new WebAdapter(driver);
    webAdapter.dropdownValue(dropdownElement, "Clothing & Accessories");
    webAdapter.implicitwait(3000);
    productListPage.enterTxtSearchbox(productname);
     webAdapter.keyboardEnterKey();
    }

    @Then("^verify that product is present in the cart$")
    public void verify_that_product_is_present_in_the_cart() throws Throwable {
     CartPage cartPage = new CartPage(driver);
     WebAdapter webAdapter = new WebAdapter(driver);

     //Add  products
     cartPage.clickOnSearchedProduct();
      //Switch to new Tab
     cartPage.navigatetoNewPage();
     webAdapter.implicitwait(9000);
     webAdapter.switchToTab(1);
     webAdapter.explicitWaitForelement("#add-to-cart-button", 5000);

        //Fluent Wait
        webAdapter.fluentWait();
        cartPage.clickAddToCartButton();

     //Verify total number of products and product name
     String strCartValue =webAdapter.getText(cartPage.CartContainerValue());
     webAdapter.assertTrue(Integer.parseInt(strCartValue));

     //Verify product name
        cartPage.clickCartContainer();
        webAdapter.implicitwait(3000);

     //close browser
     webAdapter.closebrowser();
    }

}
